/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Courtejaire Tom                                              *
*                                                                             *
*  Nom-prénom2 : Ascencio Masao                                               *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include<stdio.h>
#include<string.h>

// J'ai essayé de faire des.c et des .h mais ça ne marchait pas donc j'ai tout remis comme avant et est laissé les .c et .h que j'ai essayé

int verifAlphanum(char *message);
void Crypter(char *message, int code);
void Decrypter(char *message , int code);
void clearBuffer();

void main() {
	char message[100];
	int choixMode;
	int cle;
	printf("Entrez votre message: \n");
	scanf("%s", message);
	printf("\n");
	// Si il y a un ou plusieurs caracteres speciaux dans le code
	if (verifAlphanum(message) == 0) {
		printf("Erreur, caractere special dans code\n");
	// Si il n'y en a pas, le programme continue
	} else {
		printf("Pas de caractere special, le code est valable\n");
        printf("\n");
        printf("Voulez vous :\n");
        printf("- Crypter un message, tapez 1\n");
        printf("- Decrypter un message, tapez 2\n");
        printf("\n");
        printf("Votre choix: ");
        clearBuffer();
        choixMode=getchar();
        printf("\n");
        if(choixMode=='1') {
            printf("Vous avec choisi de crypter le message suivant: %s, veuillez indiquez maintenant la cle:\n", message);
            clearBuffer();
            printf("Cle: ");
            cle=getchar();
            // Je ne sais pas pourquoi mais quand on ecrit un chiffre dans le getchar juste avant cela rajout 48 donc je les supprime;
            cle-=48;
            printf("\n");
            Crypter(message, cle);
            printf("Le message crypte est: %s\n", message);
        } else {
            if(choixMode=='2') {
                printf("Vous avec choisi de decrypter le message suivant: %s, veuillez indiquez maintenant la cle:\n", message);
                clearBuffer();
                printf("Cle: ");
                cle=getchar();
                cle-=48;
                printf("\n");
                Decrypter(message, cle);
                printf("Le message decrypte est: %s\n", message);
            } else {
            printf("Vous n'avez pas choisi de mode de decryptage, veuillez recommencer");
            }
        }
	}
}

// fonction permettant de verifier si les caracteres sont speciaux ou non
int verifAlphanum(char *message) {
    char nb[] = "1234567890";
    // boucle pour verifier chaque caractere du message
    for(int i = 0; i < strlen(message); i++){
    	// boucle qui verifie pour un caractere si il est special ou normal
        for(int j = 0; j < strlen(nb); j++){
        	// si le caractere est un chiffre, on renvoie l'entier zero qui emmenera vers la fin du programme dans le main
            if(message[i] == nb[j]) {
                    return (0);
            }
        }
    }
    // si il n'y a aucun caractère spécial, on renvoie 1 qui enclenchera la suite du programme
    return (1);
}

void Crypter(char *message, int code) {
    int i = 0;
	char c;
    while(message[i] != '\0') {
        if (message[i] >='A' && message[i] <='Z') {
            c = message[i] - 'A';
            c+= code;
            c = c % 26;
            message[i]= c + 'A';
            i++;
        }
        if (message[i] >='a' && message[i] <='z') {
            c = message[i] - 'a';
            c+= code;
            c = c % 26;
            message[i]= c + 'a';
            i++;
        }
    }
}


void Decrypter(char *message, int code) {
	int i = 0;
	char c;
    while(message[i] != '\0') {
        if (message[i] >='A' && message[i] <='Z') {
            c = message[i] - 'A';
            c-= code;
            c = c % 26;
            message[i]= c + 'A';
            i++;
        }
        if (message[i] >='a' && message[i] <='z') {
            c = message[i] - 'a';
            c-= code;
            c = c % 26;
            message[i]= c + 'a';
            i++;
        }
    }
}


void clearBuffer(){
    char c;
    c = getchar();
    while('\n' != c){
        c = getchar();
    }
}
