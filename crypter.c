/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Courtejaire Tom                                              *
*                                                                             *
*  Nom-pr�nom2 : Ascencio Masao                                               *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include<stdio.h>
#include<string.h>
#include"crypter.h"

void Crypter(char *message, int code) {
    int i = 0;
	char c;
    while(message[i] != '\0') {
        if (message[i] >='A' && message[i] <='Z') {
            c = message[i] - 'A';
            c+= code;
            c = c % 26;
            message[i]= c + 'A';
            i++;
        }
        if (message[i] >='a' && message[i] <='z') {
            c = message[i] - 'a';
            c+= code;
            c = c % 26;
            message[i]= c + 'a';
            i++;
        }
    }
}
