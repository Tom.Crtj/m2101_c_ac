
/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Courtejaire Tom                                              *
*                                                                             *
*  Nom-pr�nom2 : Ascencio Masao                                               *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : verifAlphaNum.h                                           *
*                                                                             *
******************************************************************************/

#include<stdio.h>
#include<string.h>

int verifAlphanum(char *message);
