/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N� de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitul� : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-pr�nom1 : Courtejaire Tom                                              *
*                                                                             *
*  Nom-pr�nom2 : Ascencio Masao                                               *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : verifAlphaNum.c                                           *
*                                                                             *
******************************************************************************/

#include<stdio.h>
#include<string.h>
#include"verifAlphaNum.h"

// fonction permettant de verifier si les caracteres sont speciaux ou non
int verifAlphanum(char *message) {
    char nb[] = "1234567890";
    // boucle pour verifier chaque caractere du message
    for(int i = 0; i < strlen(message); i++){
    	// boucle qui verifie pour un caractere si il est special ou normal
        for(int j = 0; j < strlen(nb); j++){
        	// si le caractere est un chiffre, on renvoie l'entier zero qui emmenera vers la fin du programme dans le main
            if(message[i] == nb[j]) {
                    return (0);
            }
        }
    }
    // si il n'y a aucun caract�re sp�cial, on renvoie 1 qui enclenchera la suite du programme
    return (1);
}
